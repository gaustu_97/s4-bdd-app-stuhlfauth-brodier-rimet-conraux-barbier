<?php

require_once __DIR__.'/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
// ajouter silm en rentrant cette commande dans votre terminale : $ composer require slim/slim:~2.0
$app = new \Slim\Slim();

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();

$app->get("/api/games/:id", function ($id) {
    $controller = new gamepedia\controllers\GameController();
    $controller->getGame($id);
})->name("games/id");

$app->get("/api/games/:id/characters", function ($id) {
    $controller = new gamepedia\controllers\GameController();
    $controller->characters($id);
})->name("games/id/characters");

$app->get("/api/games/:id/comments", function ($id) {
    $controller = new gamepedia\controllers\GameController();
    $controller->comments($id);
})->name("games/id/comments");

$app->get("/api/games", function () {
    $controller = new gamepedia\controllers\GameController();
    $controller->getGames();
})->name("games");

$app->get("/api/characters/:id", function ($id) {
    $controller = new gamepedia\controllers\CharacterController();
    $controller->character($id);
})->name("characters/id");

$app->get("/api/comments/:id", function ($id) {
    $controller = new gamepedia\controllers\CommentController();
    $controller->comment($id);
})->name("comments/id");

$app->post("/api/games/:id/comments", function ($id) {
    $controller = new gamepedia\controllers\GameController();
    $controller->addComment($id);
});

$app->run();