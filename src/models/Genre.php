<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:34
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = "genre";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function games() {
        return $this->belongsToMany("gamepedia\models\Game", "game2genre", "genre_id", "game_id");
    }
}