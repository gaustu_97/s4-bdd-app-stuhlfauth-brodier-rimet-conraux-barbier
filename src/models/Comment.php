<?php

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "commentaire";
    protected $primaryKey = "id";
    public $timestamps = true;

    public function user() {
        return $this->belongsTo("gamepedia\models\User", "email");
    }

    public function game() {
        return $this->belongsTo("gamepedia\models\Game", "id_game");
    }
}