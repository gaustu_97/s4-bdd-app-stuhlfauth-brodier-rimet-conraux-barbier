<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 03/04/2018
 * Time: 14:48
 */

namespace gamepedia\controllers;

use gamepedia\models\Comment;

class CommentController
{
    public function comment($id) {
        return json_encode(Comment::find($id));
    }

    public function getCommGame($idGame){
        return json_encode(Comment::game()->where('game_id','=',$idGame));
    }
}