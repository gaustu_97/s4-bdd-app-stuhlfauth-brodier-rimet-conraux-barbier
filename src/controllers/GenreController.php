<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 03/04/18
 * Time: 14:46
 */

namespace gamepedia\controllers;

use gamepedia\models\Genre;

class GenreController
{
    public function genre($id) {
        return json_encode(Genre::find($id));
    }
}