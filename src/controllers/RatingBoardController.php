<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 03/04/18
 * Time: 15:14
 */

namespace gamepedia\controllers;

use gamepedia\models\RatingBoard;

class RatingBoardController
{
    public function ratingboard($id) {
        return json_encode(RatingBoard::find($id));
    }
}