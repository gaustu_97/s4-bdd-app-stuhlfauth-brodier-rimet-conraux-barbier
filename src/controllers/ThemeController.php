<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 03/04/18
 * Time: 15:15
 */

namespace gamepedia\controllers;

use gamepedia\models\Theme;

class ThemeController
{
    public function theme($id) {
        return json_encode(Theme::find($id));
    }
}