<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 03/04/2018
 * Time: 14:49
 */

namespace gamepedia\controllers;

use gamepedia\models\Company;

class CompanyController
{
    public function company($id) {
        return json_encode(Company::find($id));
    }
}