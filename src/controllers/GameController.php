<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 03/04/2018
 * Time: 14:49
 */

namespace gamepedia\controllers;

use gamepedia\models\Game;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GameController
{
    public function getGames(){
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        // Initialisation de l'offset, c'est-a-dire du nombre de photos par page
        $offset = 200;
        $pageNumber = $app->request->get("page");
        if(!isset($pageNumber)) {
            $pageNumber = 1;
        }
        try {
            $gamesArray = [];
            // Gestion de la pagination
            $games = Game::skip(($pageNumber-1) * $offset)->take($offset)->select('id', 'name', 'alias', 'deck')->get();
            foreach($games as $game) {
                // Ajout de chaque jeu dans le tableau gamesArray
                array_push($gamesArray, array("game" => $game, "links" => array("self" => array("href" => $app->urlFor("games/id", ['id' => $game->id])))));
            }
            $prev = $pageNumber - 1;
            $next = $pageNumber + 1;
            if ($pageNumber == 1) {
                $prev = 1;
            } else if ($pageNumber == (Game::count()/$offset)) {
                $next = (Game::count())/$offset;
            }
            $jsonArray = array("games" => $gamesArray, "links" => array("prev" => array("href" => $app->urlFor("games")."?page=".$prev), "next" => array("href" => $app->urlFor("games")."?page=".$next)));
        } catch (ModelNotFoundException $e) {
            $app->response->setStatus(404);
            echo json_encode(["msg" => "no games"]);
        }
        echo json_encode($jsonArray);
    }

    public function getGame($id) {
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        try {
            $g = Game::select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')->where('id', '=', $id)->firstOrFail();
            $json = array("game" => $g->toArray(), "links" => array("comments" => array("href" => $app->urlFor("games/id/comments", ["id" => $g->id])), "characters" => array("href" => $app->urlFor("games/id/characters", ["id" => $g->id]))));
        } catch (ModelNotFoundException $e) {
            $app->response->setStatus(404);
            echo json_encode(["msg" => "game $id not found"]);
        }
        echo json_encode($json);
    }

    public function comments($id){
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        try {
            $coms = Game::find($id)->comments()->get();
            $commentsArray = [];
            foreach ($coms as $com) {
                $enc = array("id" => $com->id, "titre" => $com->titre, "contenu" => $com->contenu, "created_at" => $com->created_at, "name" => $com->user()->firstOrFail()->name);
                array_push($commentsArray, $enc);
            }
        } catch (ModelNotFoundException $e) {
            $app->response->setStatus(404);
            echo json_encode(["msg" => "no comment found"]);
        }
        echo json_encode($commentsArray->toArray());
    }

    public function characters($id) {
        $app = \Slim\Slim::getInstance();
        $app->response->headers->set('Content-Type', 'application/json');
        try {
            $characters = Game::find($id)->characters()->get();
            $charactersArray = [];
            foreach ($characters as $character) {
                array_push($charactersArray, array("character" => $character, "links" => array("self" => array("href" => $app->urlFor("characters/id", ["id" => $character->id])))));
            }
            $charactersArray = array("characters" => $charactersArray);
        } catch (ModelNotFoundException $e) {
            $app->response->setStatus(404);
            echo json_encode(["msg" => "no characters found"]);
        }
        echo json_encode($charactersArray);
    }

    public function addComment($gameId) {
        $app = \Slim\Slim::getInstance();
        $body = $app->request->getBody();

        $body = json_decode($body, true);

        $comment = new \gamepedia\models\Comment();
        if(isset($body["titre"]) && isset($body["contenu"]) && isset($body["email"])) {
            $comment->titre = $body["titre"];
            $comment->contenu = $body["contenu"];
            $comment->email = $body["email"];

            $game = \gamepedia\models\Game::find($gameId);
            $game->comments()->save($comment);

            $app->response->headers->set('Location', $app->urlFor("comments/id", ["id" => $comment->id]));
            $app->response->setStatus(201);
            $app->response->headers->set('Content-Type', 'application/json');
            $commentController = new \gamepedia\controllers\CommentController();
            $app->response->write($commentController->comment($comment->id));
        } else {
            $missingColumn="";
            if(!isset($body["titre"])) {
                $missingColumn .= " titre ";
            }
            if (!isset($body["contenu"])) {
                $missingColumn .= " contenu ";
            }
            if (!isset($body['email'])) {
                $missingColumn .= " email ";
            }
            $app->response->setStatus(400);
            $app->response->write(json_encode(['msg' => $missingColumn . ' missing']));
        }
    }
}