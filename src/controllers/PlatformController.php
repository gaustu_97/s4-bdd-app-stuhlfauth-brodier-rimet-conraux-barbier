<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 03/04/18
 * Time: 15:13
 */

namespace gamepedia\controllers;

use gamepedia\models\Platfrom;

class PlatformController
{
    public function platform($id) {
        return json_encode(Platform::find($id));
    }
}