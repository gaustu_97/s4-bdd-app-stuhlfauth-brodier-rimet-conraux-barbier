<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 03/04/2018
 * Time: 14:45
 */

namespace gamepedia\controllers;

use gamepedia\models\Character;

class CharacterController
{
    public function character($id) {
        return json_encode(Character::find($id));
    }

    public function getCharacterGame($idGame){
        return json_encode(Character::games()->where('game_id','=',$idGame));
    }
}