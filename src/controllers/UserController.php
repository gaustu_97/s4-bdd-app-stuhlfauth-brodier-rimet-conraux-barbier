<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 03/04/18
 * Time: 15:16
 */

namespace gamepedia\controllers;

use gamepedia\models\User;

class UserController
{
    public function user($id) {
        return json_encode(User::find($id));
    }
}