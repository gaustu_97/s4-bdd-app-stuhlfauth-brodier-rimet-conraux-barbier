<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 03/04/2018
 * Time: 14:49
 */

namespace gamepedia\controllers;

use gamepedia\models\GameRating;

class GameRatingController
{
    public function gamerating($id) {
        return json_encode(GameRating::find($id));
    }
}