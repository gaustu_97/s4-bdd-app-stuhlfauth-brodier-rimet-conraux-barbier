<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:35
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class Platfrom extends Model
{
    protected $table = "platform";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function company() {
        return $this->belongsTo("gamepedia\models\Company", "company_id");
    }

    public function games() {
        return $this->belongsToMany("gamepedia\models\Game", "game2platform", "platform_id", "game_id");
    }
}