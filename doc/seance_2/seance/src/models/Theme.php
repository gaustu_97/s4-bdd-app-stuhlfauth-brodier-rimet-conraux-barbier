<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:36
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $table = "theme";
    protected $primaryKey = "id";
    public $timestamps=false;
    
    public function games() {
        return $this->belongsToMany("gamepedia\models\Game", "game2theme", "theme_id", "game_id");
    }
}