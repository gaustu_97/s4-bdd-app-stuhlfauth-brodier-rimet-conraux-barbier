<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:24
 */

namespace gamepedia\models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = "game";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function publishers() {
        return $this->belongsToMany("gamepedia\models\Company", "game_publishers", "game_id", "comp_id");
    }

    public function developers() {
        return $this->belongsToMany("gamepedia\models\Company", "game_developers", "game_id", "comp_id");
    }

    public function characters() {
        return $this->belongsToMany("gamepedia\models\Character", "game2character", "game_id", "character_id");
    }

    public function platforms() {
        return $this->belongsToMany("gamepedia\models\Platform", "game2platform", "game_id", "platform_id");
    }

    public function themes() {
        return $this->belongsToMany("gamepedia\models\Theme", "game2theme", "game_id", "theme_id");
    }

    public function genres() {
        return $this->belongsToMany("gamepedia\models\Genre", "game2genre", "game_id", "genre_id");
    }

    public function ratings() {
        return $this->belongsToMany("gamepedia\models\GameRating", "game2rating", "game_id", "rating_id");
    }

    public function similar_games() {
        return $this->belongsToMany("gamepedia\models\Game", "similar_games", "game1_id", "game2_id");
    }

    public function first_appeared_in_game() {
        return $this->hasMany("gamepedia\models\Character", "first_appeared_in_game_id");
    }
}