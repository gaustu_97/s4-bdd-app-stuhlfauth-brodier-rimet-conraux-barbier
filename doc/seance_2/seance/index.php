<?php

require_once __DIR__.'/vendor/autoload.php';


use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();

echo "<h1>Séance 2</h1></br>";
echo "<h2>Question 1</h2>";
$personnages = gamepedia\models\Game::find(12342)->characters()->get();
echo "<p>Les personnages du jeu " . gamepedia\models\Game::find(12342)->name . " sont :</p><ul>";
foreach($personnages as $perso) {
    echo "<li>" . $perso->name . " : " . $perso->deck . "</li>";
}
echo "</ul>";



echo "<h2>Question 2</h2>";
$g=\gamepedia\models\Game::where("name","like","Mario%")->get();
echo "<p>les personnages des jeux dont le nom (du jeu) débute par 'Mario' : </p><ul>";
foreach ($g as $game){
    $c=$game->characters()->get();
    echo "<li>".$game->name."<ul>";
    foreach ($c as $char){
        echo "<li>".$char->name."</li>";
    }
    echo "</ul></li>";
}
echo "</ul>";

echo "<h2>Question 3</h2>";
echo "<p>les jeux développés par une compagnie dont le nom contient 'Sony' : </p><ul>";
$c=\gamepedia\models\Company::where("name","like","%Sony%")->get();
foreach ($c as $com){
    echo "<li>".$com->name."<ul>";
    $g=$com->developers()->get();
    foreach ($g as $game){
        echo "<li>".$game->name."</li>";
    }
    echo "</ul></li>";
}
echo "</ul>";

echo "<h2>Question 4</h2>";
echo "<p>le rating initial (indiquer le rating board) des jeux dont le nom contient Mario : </p><ul>";
$ex4= \gamepedia\models\Game::where("name","like","%mario%")->get();
foreach ($ex4 as $game){
    $ex41=$game->ratings()->get();
    echo "<li>$game->name : <ul>";
    foreach ($ex41 as $rating) {
        echo "<li>".$rating->name ." : ".$rating->ratingboard()->first()["deck"]."</li>";
    }
    echo "</ul></li>";
}
echo "</ul>";

echo "<h2>Question 5</h2>";
echo "<p>Les jeux dont le nom débute par Mario, qui ont plus de 3 personnages :</p><ul>";
$test2 = \gamepedia\models\Game::where("name", "like", "Mario%")->has("characters", ">", 3)->get();
foreach ($test2 as $game){
    echo "<li>".$game->name."</li>";
}
echo "</ul>";

echo "<h2>Question 6</h2>";
echo "<p>Les jeux dont le nom débute par Mario, publiés par une compagnie dont le rating initial contient \"3+\" sont :</p><ul>";
$q6 = gamepedia\models\Game::where("name","like","Mario%")
    ->whereHas("ratings",function($ql){
    $ql->where("name","like","%3+%");
})->get();
foreach ($q6 as $game){
    echo " <li> ". $game->name ."</li>";
}
echo "</ul>";


echo "<h2>Question 7</h2>";
$games = gamepedia\models\Game::where("name", "like", "Mario%")
                ->whereHas("publishers", function ($g) {
                    $g->where("name", "like", "%Inc.%");
                })->whereHas("ratings", function ($g) {
                    $g->where("name", "like", "%3+");
                })->get();
echo "<p>Les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient 
\"Inc.\" et dont le rating initial contient \"3+\" sont :</p><ul>";
foreach($games as $game) {
    echo "<li>" . $game->name . "</li>";
}
echo "</ul>";

echo "<h2>Question 8</h2>";
$games = gamepedia\models\Game::where("name", "like", "Mario%")
    ->whereHas("publishers", function ($g) {
        $g->where("name", "like", "%Inc.%");
    })->whereHas("ratings", function ($g) {
        $g->where("name", "like", "%3+");
    })->whereHas("ratings", function ($g) {
        $g->whereHas("ratingboard", function ($m) {
            $m->where("name", "like", "CERO");
        });
    })->get();
echo "<p>Les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient 
\"Inc.\" et dont le rating initial contient \"3+\" et ayant reçu un avis de la part du rating board nommé
\"CERO\" sont :</p><ul>";
foreach($games as $game) {
    echo "<li>" . $game->name . "</li>";
}
echo "</ul>";


echo "<h2>Question 9</h2>";
echo "Ajout du genre Date (Love)";
$genre = new gamepedia\models\Genre();
$genre->name = "Date";
$genre->save();
$genre->games()->sync([12, 56, 12, 345]);