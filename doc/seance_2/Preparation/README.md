﻿# Bases de données et applications - Préparation à la séance 2

## Schéma SQL

Categorie(**id**, nom, descr)  
Annonce(**id**, titre, date, texte)  
CategorieToAnnonce(***categ_id, annonce_id***)  
Photo(**id**, file, date, taille_octet, *annonce_id*)

## Classe Annonce

    <?php  
    namespace cours\models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Annonce extends Model {
	    protected $table = "annonce";
	    protected $primaryKey = "id"; 
	    public $timestamps = false; 
	    
	    public function photos() { 
		    return $this->hasMany(“cours\models\Photo”, “annonce_id”);  
		}
		
		public function categories() {
			return $this->belongsToMany(“cours\models\Categorie”, “categorietoannonce”, “annonce_id”, “categ_id”);
		}
	}

## Classe Photo

    <?php  
    namespace cours\models;
    
    use Illuminate\Database\Eloquent\Model;
    
    class Photo extends Model {  
	    protected $table = "photo";
	    protected $primaryKey = "id";  
	    public $timestamps = false;  
      
	    public function annonce() {  
		    return $this->belongsTo(“cours\models\Annonce”, “annonce_id”);  
	    }
    }

## Requêtes Eloquent
### Photos de l'annonce 22

    $ex1 = cours\models\Annonce::find(22)->photos()->get();

### Photos de l'annonce 22 dont la taille en octets est supérieure à 100000

    $ex1 = cours\models\Annonce::find(22)
		    ->photos()  
		    ->where("taille_octet", ">", “100000”)  
		    ->get();

### Annonces possédant plus de 3 photos

    $ex1 = cours\models\Annonce::has(“photos”, ">", 3)->get();

### Annonces possédant   des photos dont la taille est supérieure à 100000

    $ex1 = cours\models\Annonce::whereHas("photos", function ($q) {
	    $q->where("taille_octet", ">", "100000");
    })->get();

### Ajouter une photo à l'annonce 22

    $p = new cours\models\Photo();
    $p->file = “img_accident_annonce22.png”;
    $p->date = “2018-03-13”;
    $p->taille_octet = 94672;
    
    $annonce = cours\models\Annonce()::find(22);
    $annonce->photos()->save($p);

### Ajouter l'annonce 22 aux catégories 42 et 73

    $annonce = cours\models\Annonce::find(22);
    $annonce->categories()->attach([42,73]);


----------
Quentin BARBIER, Jérémy BRODIER, Kylian CONRAUX, Quentin RIMET, Gautier STUHLFAUTH
S4 SI2
