# S4-BDD-App-Stuhlfauth-Brodier-Rimet-Conraux-Barbier
#Scéance 1 : TD

![alt text](Uml.png)
<b>MODELE RELATIONNEL</b>

Jeu Video (``idJeu``,nom,desc_courte,desc_longue,_date_sortie_initial,date_sortie_attendu)

Genre(``idGenre``,nom,desc_courte,desc_longue)

Theme(``idTheme``,nom)

Classement(``idClass``,classement,#idOrga)

Organisme(``idOrga``,nom,desc_courte,desc_longue)

Plateforme(``idPlat``,nom,alias,desc_courte,desc_longue,date_sortie,tarif_initial,decompte,#idComp)

Compagnie(``idComp``,nom,alias,abreviation,desc_courte,desc_longue,adresse,date_creation,num_tel,url)

Personnage(``idPers``,nom,alias,nom_reel,nom_famille,date_naiss,genre,des_courte,desc_longue,#idPremierJeu)

EstGenre(``#idJeu,#idGenre``)

EstTheme(``#idJeu,#idTheme``)

EstClasse(``#idJeu,#idClass``)

EstSurPlat(``#idJeu,#idPlat``)

EstPublie(``#idJeu,#idComp``)

EstDeveloppe(``#idJeu,#idComp``)

EstPersonnage(``#idJeu,#idPers``)

EstEnnemie(``#idPers1,#idPers2``)

EstAllié(``#idPers1,#idPers2``)
