<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:23
 */

namespace gamepedia\models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "company";
    protected $primaryKey = "id";
    public $timestamps=false;

}