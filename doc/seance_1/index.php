<?php

require_once __DIR__.'/vendor/autoload.php';


use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();


echo "<h1>Exercice 1</h1></br>";

$ex1= \gamepedia\models\Game::where("name","like","%mario%")->get();
foreach ($ex1 as $game){
    echo $game->name."<br/>";
}
echo "<h1>Exercice 2</h1></br>";

$ex2= \gamepedia\models\Company::where("location_country","=","japan")->get();
foreach ($ex2 as $com){
    echo $com->name."<br/>";
}

echo "<h1>Exercice 3</h1></br>";

$ex3= \gamepedia\models\Platfrom::where("install_base",">=","10000000")->get();
foreach ($ex3 as $com){
    echo $com->name." : ".$com->install_base."<br/>";
}

echo "<h1>Exercice 4</h1></br>";

$ex4= \gamepedia\models\Game::skip(21172)->take(442)->get();
foreach ($ex4 as $game){
    echo $game->name." : ".$game->id."<br/>";
}

echo "<h1>Exercice 5</h1></br>";

if(isset($_GET['p']))
    $page=$_GET['p'];
else $page=1;

$ex5= \gamepedia\models\Game::skip(($page-1)*500)->take(500)->get();
foreach ($ex5 as $game) {
    echo $game->name . " : " . $game->deck . "<br/>";
}
$nbGame=\gamepedia\models\Game::count();
for($i=1;$i<=ceil($nbGame/500);$i++){
    echo "<a href=\"index.php?p=$i\">$i</a> / ";}