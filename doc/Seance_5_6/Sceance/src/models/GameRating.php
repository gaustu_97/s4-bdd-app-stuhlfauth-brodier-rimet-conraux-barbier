<?php

namespace gamepedia\models;

use Illuminate\Database\Eloquent\Model;

class GameRating extends Model
{
    protected $table = "game_rating";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function games() {
        return $this->belongsToMany("gamepedia\models\Game", "game2rating", "rating_id", "game_id");
    }

    public function ratingboard() {
        return $this->belongsTo("gamepedia\models\RatingBoard", "rating_board_id");
    }
}