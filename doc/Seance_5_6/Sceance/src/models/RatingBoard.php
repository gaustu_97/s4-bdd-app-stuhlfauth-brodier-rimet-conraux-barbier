<?php
/**
 * Created by PhpStorm.
 * User: brodi
 * Date: 07/03/18
 * Time: 16:38
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class RatingBoard extends Model
{
    protected $table = "rating_board";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function gameratings() {
        return $this->hasMany("gamepedia\models\GameRating", "rating_board_id");
    }


}