﻿# Bases de données et applications - Préparation à la séance 2

## Lorsque la fonction json_encode() reçoit un tableau PHP :    

    
    elle retourne un tableau '[...]' quand elle reçoit un tableau php avec des clés entieres
    elle retourne un objet '{...}' quand elle reçoit un objet ou un tableau associatif
    
## En utilisant le micro-framework slim, comment accède-t-on aux données transmises dans la requête sans utiliser les tableau $_GET et $_POST :

    pour récupérer les données dans l'url:
    {
    <?php
    $paramValue = $app->request()->get('paramName');
    $paramValue = $app->request()->post('paramName');
    }
      
    pour récupérer les données dans le corp de la requete:
    {
    <?php
    $body = $app->$request->getBody();
    }
 
## en utilisant slim, comment positionner le code de retour de la réponse (200, 404, 401 …) et un header dans la réponse :
    $app->response->setStatus("400"): 
    positionne le status
    code de la réponse
    –
    $app->response->header->set('Content-type','application/json') : ajoute la chaine dans la partie header de
    la réponse – la chaine doit correspondre à un header      
----------
Quentin BARBIER, Jérémy BRODIER, Kylian CONRAUX, Quentin RIMET, Gautier STUHLFAUTH
S4 SI2
