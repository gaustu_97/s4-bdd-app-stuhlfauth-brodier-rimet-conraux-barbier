# Bases de données et applications - Préparation à la séance 3

## Partie 1 : mesurer les performances et utiliser des index
### Code pour mesurer le temps d'exécution d'une séquence d'instructions PHP

    <?php  
    $beginTime = microtime(true);  
    …  
    $endTime = microtime(true) - $beginTime;

### Les index
#### Intérêt
Les index sont utilisés pour accélérer les requêtes (notamment les requêtes impliquant plusieurs tables, ou les requêtes de recherche), et sont indispensables à la création de clés, étrangères et primaires, qui permettent de garantir l'intégrité des données de la base et dont nous discuterons au chapitre suivant.
#### Principe de fonctionnement
Lorsqu’on crée un index sur une table, MySQL stocke cet index sous forme d'une structure particulière, contenant les valeurs des colonnes impliquées dans l'index. Cette structure stocke les valeurs triées et permet d'accéder à chacune de manière efficace et rapide.
![Principe de fonctionnement d'un index](https://user.oc-static.com/files/375001_376000/375841.png)

## Partie 2 : observer l'orm, améliorer les performances avec des chargements liés
### Structure de log des requêtes avec Eloquent
C'est un tableau ayant la taille du nombre de requêtes faites, dans chacune de ces cases on retrouve un tableau possédant la requête prépare, les valeurs et son temps d'exécution.
### Problème des N+1 queries
Le problème des N+1 queries est que lorsqu’on veut obtenir la valeur d’un attribut d’une table B dépendante d’une autre A pour chaque tuple de la table A, nous sommes obligés de faire une requête pour obtenir tous les tuples de cette table A, puis effectuer une autre requête SQL pour obtenir, pour chaque tuple trouvé, la valeur de l’attribut de la table B. Cela nous mène donc à avoir N+1 requêtes pour N tuples de la table A.  
  
On peut le faire uniquement avec deux requêtes grâce au chargement lié.
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTc0NTgxNjU0XX0=
-->