# Bases de données et applications - Séance 3

### Cache Sql

Le cache Sql permet de grandement réduire le temps d'exécutions d'une requête couteuse

### Ajout d'index
L'ajout d'un index sur le nom améliore légèrement la vitesse d'exécution des requêtes lorsque le nom recherché est en début de valeur

L'ajout d'un index sur le nom n'améliore pas la vitesse d'exécution des requêtes lorsque le nom recherché n'est pas en début de valeur car il doit quand même rechercher tous les débuts de valeur

L'ajout d'un index sur location_country n'améliore pas la vitesse d'exécution des requêtes car il n'y a pas assez de valeurs différentes sur location_country

### Chargements liés

Seules deux requêtes sont effectuées contre 156 avant le chargement lié.

`Select * from game where name like "mario";`

`Select character.*, game2character.game_id as pivot_game_id, game2character.character_id as pivot_character_id from character inner join game2character on character.id=game2character.character_id where game2character.game_id in ("id des jeux recuperes")`