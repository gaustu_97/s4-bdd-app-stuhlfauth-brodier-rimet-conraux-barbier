<?php

require_once __DIR__.'/vendor/autoload.php';


use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();

echo "<h1>Séance 3</h1></br>";
echo "<h2>Partie 1</h2>";
echo "<h3>Question 1</h3>";

$beginTime = microtime(true);
$q1 = gamepedia\models\Game::all()->toArray();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";


echo "<h3>Question 2</h3>";
$beginTime = microtime(true);
$q2= \gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";


echo "<h3>Question 3</h3>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","Mario%")->get();
foreach ($g as $game){
    $c=$game->characters()->get();
}
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<h3>Question 4</h3>";
$beginTime = microtime(true);
$q6 = gamepedia\models\Game::where("name","like","Mario%")
    ->whereHas("ratings",function($ql){
        $ql->where("name","like","%3+%");
    })->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";
echo "<h3>Cache Sql</h3>";

$beginTime = microtime(true);
$q2= \gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

$beginTime = microtime(true);
$q2= \gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

$beginTime = microtime(true);
$q2= \gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<h3>lister les jeux dont le nom débute par 'valeur'</h3>";

echo "<p>Valeur 1 : Mario </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : Desert </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","Desert%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 3 : \"WWE\" </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","WWE%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>--Ajout d'un index--</p>";
$t=DB::connection()->table("game",function ($table){
    $table->index("name");
});

echo "<p>Valeur 1 : Mario </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : Desert </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","Desert%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 3 : \"WWE\" </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","WWE%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

$t=DB::connection()->table("game",function ($table){
    $table->dropindex("name");
});


echo "<h3>lister les jeux dont le nom contient 'valeur'</h3>";

echo "<p>Valeur 1 : Mario </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : Desert </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%Desert%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 3 : \"WWE\" </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%WWE%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>--Ajout d'un index--</p>";
$t=DB::connection()->table("game",function ($table){
    $table->index("name");
});


echo "<p>Valeur 1 : Mario </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%Mario%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : Desert </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%Desert%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 3 : \"WWE\" </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Game::where("name","like","%WWE%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";



echo "<h3>lister les compagnie qui sont dans le pays qui commence par 'valeur'</h3>";

echo "<p>Valeur 1 : US </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Company::where("location_country","like","US%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : France </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Company::where("location_country","like","France%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>--Ajout d'un index--</p>";
$t=DB::connection()->table("company",function ($table){
    $table->index("location_country");
});


echo "<p>Valeur 1 : US </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Company::where("location_country","like","US%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";

echo "<p>Valeur 2 : France </p>";
$beginTime = microtime(true);
$g=\gamepedia\models\Company::where("location_country","like","France%")->get();
$endTime = microtime(true) - $beginTime;
echo "<p>". $endTime."</p>";


echo "<h2>Partie 2</h2>";

DB::connection()->enableQueryLog();
DB::connection()->flushQueryLog();

echo "<h3>Question 1</h3>";
$q1=\gamepedia\models\Game::where("name","like","%mario%")->get();
$querie=DB::getQueryLog();
echo count($querie);

echo "<h3>Question 2</h3>";
DB::connection()->flushQueryLog();
$g=\gamepedia\models\Game::find(12342);
$g->characters()->get();
$querie=DB::getQueryLog();
echo count($querie);
echo "<h3>Question 3</h3>";

DB::connection()->flushQueryLog();
$g=\gamepedia\models\Game::where("name","like","%Mario%")->get();
foreach ($g as $game){
    $c=$game->first_appeared_in_game()->get();
}
$querie=DB::getQueryLog();
echo count($querie);

echo "<h3>Question 4</h3>";
DB::connection()->flushQueryLog();
$g=\gamepedia\models\Game::where("name","like","%Mario%")->get();
foreach ($g as $game){
    $c=$game->characters()->get();
}
$querie=DB::getQueryLog();
echo count($querie);

echo "<h3>Question 5</h3>";
DB::connection()->flushQueryLog();
$c=\gamepedia\models\Company::where("name","like","%Sony%")->get();
foreach ($c as $com){
    $g=$com->developers()->get();
}
$querie=DB::getQueryLog();
echo count($querie);


echo "<h3>Chargements liés</h3>";
DB::connection()->flushQueryLog();
$g=\gamepedia\models\Game::with('characters')->where("name","like","%Mario%")->get();
$querie=DB::getQueryLog();
echo count($querie);
echo "<h3>Chargements liés 2</h3>";
DB::connection()->flushQueryLog();
$g=\gamepedia\models\Company::with('developers')->where("name","like","%sony%")->get();
$querie=DB::getQueryLog();
echo count($querie);