﻿# Bases de données et applications - Préparation à la séance 4

## Comment installer faker ?

    {
	    "require": {
		    "illuminate/database": "5.5.*",
		    “fzaninotto/faker”:”*”,
		    "php": ">=7.0.0"
	},

## Donnez un exemple de code pour générer une adresse américaine en utilisant faker

    <?php
    require_once '/path/to/Faker/src/autoload.php';
    $faker = Faker\\Factory::create();
    
    echo $faker->address;
    // "426 Jordy Lodge
    // Cartwrightshire, SC 88120-6700"

## Formatez une date en type DateTime : "2017/02/16 (16:15)"

    <?php
    $d = DateTime::createFromFormat("Y/m/d (H:i)", "2017/02/16 (16:15)");

