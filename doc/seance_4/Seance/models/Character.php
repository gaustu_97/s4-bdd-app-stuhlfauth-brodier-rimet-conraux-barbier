<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 20/11/2017
 * Time: 11:28
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = "character";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function games() {
        return $this->belongsToMany("gamepedia\models\Game", "game2character", "character_id", "game_id");
    }

    public function enemies() {
        return $this->belongsToMany("gamepedia\models\Character", "enemies", "char1_id", "char2_id");
    }
    
    public function friends() {
        return $this->belongsToMany("gamepedia\models\Character", "friends", "char1_id", "char2_id");
    }

    public function first_appeared_in_game() {
        return $this->belongsTo("gamepedia\models\Game", "first_appeared_in_game_id");
    }
}