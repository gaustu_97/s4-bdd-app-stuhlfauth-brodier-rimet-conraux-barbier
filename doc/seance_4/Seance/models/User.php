<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/03/18
 * Time: 14:22
 */

namespace gamepedia\models;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
    protected $primaryKey = "email";
    public $timestamps=false;

    public $incrementing = false;
    public $keyType = 'string';

    public function commentaires() {
        return $this->hasMany("gamepedia\models\Comment", "email");
    }
}