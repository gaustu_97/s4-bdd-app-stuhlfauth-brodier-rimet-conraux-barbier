<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/03/18
 * Time: 15:23
 */

namespace gamepedia\models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "company";
    protected $primaryKey = "id";
    public $timestamps=false;

    public function publishers() {
        return $this->belongsToMany("gamepedia\models\Game", "game_publishers", "comp_id", "game_id");
    }

    public function developers() {
        return $this->belongsToMany("gamepedia\models\Game", "game_developers", "comp_id", "game_id");
    }

    public function platforms() {
        return $this->hasMany("gamepedia\models\Platform", "company_id");
    }
}