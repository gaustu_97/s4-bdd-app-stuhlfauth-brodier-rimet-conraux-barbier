<?php

require_once __DIR__.'/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\models\User;
use gamepedia\models\Game;
use gamepedia\models\Comment;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();

echo "<h1>Séance 4</h1></br>";
/*
$gautier = new User();
$gautier->email = "gaustu@gmail.com";
$gautier->family_name = "Stuhlfauth";
$gautier->name = "Gautier";
$gautier->address = "9, Rue St Ferréol 57050 METZ";
$gautier->phone = "03 04 05 06 07";
$gautier->birthday = new DateTime("1997-03-27");
$gautier->save();

$kylian = new User();
$kylian->email = "kylian.conraux@hotmail.com";
$kylian->family_name = "Conraux";
$kylian->name = "Kylian";
$kylian->address = "83, rue de Raymond Poincaré 92200 NEUILLY-SUR-SEINE";
$kylian->phone = "06 01 02 03 04";
$kylian->birthday = new DateTime("1998-01-01");
$kylian->save();

$comment1 = new Comment();
$comment1->titre = "Un faux commentaire";
$comment1->contenu = "Loin, très loin, au delà des monts Mots, à mille lieues des pays Voyellie et Consonnia, demeurent les Bolos Bolos.";
$comment1->created_at = new DateTime();
$comment1->updated_at = new DateTime();
$comment1->save();

$comment2 = new Comment();
$comment2->titre = "Autre commentaire fake";
$comment2->contenu = "Soudain, David eut une idée. Florence jouait un rôle fondamental dans cette histoire, mais elle ne pouvait pas connaître les conséquences de ses actes.";
$comment2->created_at = new DateTime();
$comment2->updated_at = new DateTime();
$comment2->save();

$comment3 = new Comment();
$comment3->titre = "Un commentaire pas réel";
$comment3->contenu = "Toutes les connaissances que les hommes avaient mises sur Internet lui étaient accessibles. Les grandes bibliothèques du monde entier n’avaient plus de secret pour lui.";
$comment3->created_at = new DateTime();
$comment3->updated_at = new DateTime();
$comment3->save();

$comment4 = new Comment();
$comment4->titre = "Un rageux";
$comment4->contenu = "Un long silence se fit dans la voiture. Le chauffeur regardait droit devant. David jeta un œil sur le compteur qui affichait 210km/h.";
$comment4->created_at = new DateTime();
$comment4->updated_at = new DateTime();
$comment4->save();

$comment5 = new Comment();
$comment5->titre = "N'importe quoi";
$comment5->contenu = "Je m’en rappellerais si j’avais créé un programme capable de parler. Et puis tiens, je suis en train de taper la causette avec un ordinateur !";
$comment5->created_at = new DateTime();
$comment5->updated_at = new DateTime();
$comment5->save();

$comment6 = new Comment();
$comment6->titre = "Sans importance";
$comment6->contenu = "« Oui, mais rien d’exceptionnel. » David essaie de se rappeler si dans la lancée de sa jeunesse fougueuse...";
$comment6->created_at = new DateTime();
$comment6->updated_at = new DateTime();
$comment6->save();

Game::find(12342)->comments()->save($comment1);
Game::find(12342)->comments()->save($comment2);
Game::find(12342)->comments()->save($comment3);
Game::find(12342)->comments()->save($comment4);
Game::find(12342)->comments()->save($comment5);
Game::find(12342)->comments()->save($comment6);

$kylian->commentaires()->save($comment1);
$kylian->commentaires()->save($comment2);
$kylian->commentaires()->save($comment3);

$gautier->commentaires()->save($comment4);
$gautier->commentaires()->save($comment5);
$gautier->commentaires()->save($comment6);

echo "<h1>Séance 4</h1></br>";
*/
$faker = Faker\Factory::create();
for($i=0;$i<25000;$i++){
    $user=new \gamepedia\models\User();
    $user->email=$faker->email();
    $user->family_name=$faker->lastName();
    $user->name=$faker->firstName();
    $user->address=$faker->address();
    $user->phone=$faker->phoneNumber();
    $user->birthday=$faker->dateTime();
    $user->save();
}


$game=\gamepedia\models\Game::all()->toArray();
$user=\gamepedia\models\User::all();
$si=count($game);
for($i=0;$i<250000;$i++){
   $id_game=rand(1,$si);
   $id_user=rand(1,25000);

   $com=new \gamepedia\models\Comment();
   $com->id_game=$id_game;
   $com->email=$user[$id_user]->email;
   $com->titre=$faker->title();
   $com->contenue=$faker->text();
   $com->created_at=$faker->dateTime();
   $com->save();
}